help:
	@echo
	@echo "Makefile Targets:"
	@echo "   help: this text"
	@echo "   linux: build cli curses binary"
	@echo "   mac: Mac OSX app"
	@echo "   run: run the last compiled app"
	@echo

run.sh: FORCE
	@echo "#!/bin/bash" > run.sh
	@chmod 755 run.sh

linux: run.sh
	cd build/linux && make curses
	@echo "bin/linux/brogue" >> run.sh

mac: run.sh
	xcodebuild -project build/macos/Brogue.xcodeproj
	@echo "open bin/macos/Brogue.app" >> run.sh

run:
	@./run.sh

clean:
	find . -iname .ds_store -delete
	find . -iname *.o -delete
	rm -rf build/macos/build

uninstall:
	rm -rf bin/macos/Brogue.app
	rm -rf bin/linux/brogue
	rm run.sh

FORCE:
