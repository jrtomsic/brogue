Brogue
======
This is a fork of [Brian Walker's "Brogue"](https://sites.google.com/site/broguegame/ "Brogue"), a wonderful roguelike game.


This Branch
===========
This branch reflects Brogue v1.7.3 as it is released officialy by Brian Walker. Source files and resources have exactly the same state, while build scripts are adapted to the unified folder structure.


Folder Layout
-------------

The build scripts for each platform can be found in `build`. They work on source code and resources (`src` and `res`) to produce binaries in `bin` for each platform.

* `src/brogue` has all the platform independent code related to brogue.
* `src/platform` contains the loaders and libraries for linux/windows tcod/console builds.
* `src/platform/macos` contains loaders and libraries specific to Objective-C builds.

The root `Makefile` is a convenience to compile and run the specific builds without having to CD into the build directory.

* See `make help` for more info.

Original credits for this folder structure go to [Sulai](https://github.com/sulai/Brogue "Sulai") with slight modifications by me.

* Clean up `.gitignore` a bit.
* Added the convenience `Makefile`.
* Fixed the build scripts for macos app.
